import whisper
import sys

path = sys.argv[1]

model = whisper.load_model("medium")

model.to('mps')

result = model.transcribe(path)
segments = result["segments"]

print(segments)