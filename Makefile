install:
	pip install -r requirements.txt
	wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2204/x86_64/cuda-keyring_1.1-1_all.deb
	sudo dpkg -i cuda-keyring_1.1-1_all.deb
	sudo apt-get update
	sudo apt-get -y install cuda-toolkit-12-4
	sudo apt-get install -y nvidia-driver-550-open
	sudo apt-get install -y cuda-drivers-550
	pip install git+https://github.com/openai/whisper.git


start:
	uvicorn main:app --host 0.0.0.0 --port 2323 --workers 1

update:
	git pull
	supervisorctl restart all

