from fastapi import FastAPI, File, UploadFile, Form
import recognize
from fastapi import FastAPI, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from starlette.responses import PlainTextResponse
from fastapi.responses import FileResponse,StreamingResponse
import tempfile
import os
import whisper
import os
import warnings
import torch
from pyannote.audio.pipelines.speaker_verification import PretrainedSpeakerEmbedding
from pyannote.audio import Audio
from pyannote.core import Segment
import wave
import contextlib
from sklearn.cluster import AgglomerativeClustering
import numpy as np
import whisper
import datetime
from pathlib import Path
import asyncio
import pickle

#large-v3
model = whisper.load_model("large-v3",'cuda',in_memory=True)
# embedding_model = PretrainedSpeakerEmbedding(
#         "speechbrain/spkrec-ecapa-voxceleb",
#         device=torch.device("cuda")
#     )

app = FastAPI(title="Big man")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

def authenticate_user(token: str = Depends(oauth2_scheme)):
    # Ваша логика проверки токена, например, проверка в базе данных
    if token != "d4e54cb51e8c5a9cce738150edcfe8b2cb7b2f7bc723070f597ac6492ab3f7cb":
        raise HTTPException(status_code=401, detail="Invalid token")
    return token

@app.get("/")
def read_root(token: str = Depends(authenticate_user)):
    return {"app": "active"}

# Защищенный маршрут, который требует аутентификации
# @app.post("/make_subtitle", response_class=PlainTextResponse)
# async def make_subtitle(current_user: str = Depends(authenticate_user),wav: UploadFile = File(...)):
#     contents = await wav.read()
#     file_copy = tempfile.NamedTemporaryFile('wb', delete=False)
#     f = None
#
#     try:
#         # Write the contents to the temp file
#         with file_copy as f:
#             f.write(contents)
#
#         path = file_copy.name
#         result = model.transcribe(path)
#         segments = result["segments"]
#
#         with contextlib.closing(wave.open(path, 'r')) as f:
#             frames = f.getnframes()
#             rate = f.getframerate()
#             duration = frames / float(rate)
#
#         audio = Audio()
#
#         def segment_embedding(segment):
#             start = segment["start"]
#             # Whisper overshoots the end timestamp in the last segment
#             end = min(duration, segment["end"])
#             clip = Segment(start, end)
#             waveform, sample_rate = audio.crop(path, clip)
#             return embedding_model(waveform[None])
#
#         embeddings = np.zeros(shape=(len(segments), 192))
#         for i, segment in enumerate(segments):
#             embeddings[i] = segment_embedding(segment)
#
#         embeddings = np.nan_to_num(embeddings)
#
#         clustering = AgglomerativeClustering(10).fit(embeddings)
#         labels = clustering.labels_
#         # print(segments)
#         for i in range(len(segments)):
#             segments[i]["speaker"] = 'SPEAKER ' + str(labels[i] + 1)
#
#         def time(secs):
#             return datetime.timedelta(seconds=round(secs))
#
#         return JSONResponse(segments)
#
#         # subtitle_path = recognize.make_subtitles(file_copy.name,wav.filename)
#         # return FileResponse(subtitle_path, media_type="application/x-subrip", filename="subtitle.srt")
#
#     finally:
#         if f is not None:
#             f.close()  # Remember to close the file
#         os.unlink(file_copy.name)  # delete the file

@app.post("/makesub", response_class=PlainTextResponse)
async def makesub(wav: UploadFile = File(...),lang: str = Form(default='ru')):
    start = datetime.datetime.now()
    contents = await wav.read()
    file_copy = tempfile.NamedTemporaryFile('wb', delete=False)
    f = None

    timeout = 8

    async def transcribe_audio(file_copy,lang):
        options = dict(task="transcribe", language=lang, condition_on_previous_text=False, fp16=True)
        return model.transcribe(file_copy.name,**options)

    srt_entry = f"WEBVTT\n\n"

    try:
        # Write the contents to the temp file
        with file_copy as f:
            f.write(contents)

        result = await asyncio.wait_for(transcribe_audio(file_copy,lang), timeout)
        segments = result["segments"]

        subtitle_number = 1

        def time(secs):
            return datetime.timedelta(seconds=round(secs))

        for (i, segment) in enumerate(segments):
            start_time = str(time(segment["start"]))
            end_time = str(time(segment["end"]))
            text = segment["text"][1:] + ' '
            #speaker = segment["speaker"]
            srt_entry += f"{start_time}.000 --> {end_time}.000\n{text}\n\n"
            subtitle_number += 1
        finish = datetime.datetime.now()

        return StreamingResponse(
            content=srt_entry,
            media_type="application/x-subrip",
            headers={"Content-Disposition": "attachment; filename=subtitle.vtt","AI-Time":str(finish - start)}
        )
        #return JSONResponse(segments)
        #return FileResponse(srt_entry, media_type="application/x-subrip", filename="subtitle.srt")
    except asyncio.TimeoutError:
        return StreamingResponse(
            content=f"WEBVTT\n\n",
            media_type="application/x-subrip",
            headers={"Content-Disposition": "attachment; filename=subtitle.srt", "AI-Time": '10'}
        )

    finally:
        if f is not None:
            f.close()  # Remember to close the file
        os.unlink(file_copy.name)  # delete the file

