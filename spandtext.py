import whisper
import datetime
import sys
import os
import subprocess
import warnings

path = sys.argv[1]
filename_origin = os.path.basename(path)[:-4]

with warnings.catch_warnings():
    warnings.simplefilter("ignore", category=UserWarning)

print_in_console = False
if "--out" in sys.argv:
    print_in_console = True

#subprocess.call(['ffmpeg', '-i', path, '-acodec','pcm_s16le','-ar','44100','-ac','1', f"{filename_origin}.wav",'-y'])

if path[-3:] != 'wav':
  subprocess.call(['ffmpeg', '-i', path,'-loglevel','quiet','-ac','1', f"./{filename_origin}.wav", '-y'])
  path = f"./{filename_origin}.wav"

# subprocess.call(['ffmpeg', '-i', path, f"{filename_origin}.wav",'-y'])

filename = f"{filename_origin}.wav"

import torch
import pyannote.audio
from pyannote.audio.pipelines.speaker_verification import PretrainedSpeakerEmbedding
embedding_model = PretrainedSpeakerEmbedding(
    "speechbrain/spkrec-ecapa-voxceleb",
    device=torch.device("cuda")
)

from pyannote.audio import Audio
from pyannote.core import Segment

import wave
import contextlib

from sklearn.cluster import AgglomerativeClustering
import numpy as np

model = whisper.load_model("medium")

result = model.transcribe(path)
segments = result["segments"]

with contextlib.closing(wave.open(path,'r')) as f:
  frames = f.getnframes()
  rate = f.getframerate()
  duration = frames / float(rate)

audio = Audio()

def segment_embedding(segment):
  start = segment["start"]
  # Whisper overshoots the end timestamp in the last segment
  end = min(duration, segment["end"])
  clip = Segment(start, end)
  waveform, sample_rate = audio.crop(path, clip)
  return embedding_model(waveform[None])

embeddings = np.zeros(shape=(len(segments), 192))
for i, segment in enumerate(segments):
  embeddings[i] = segment_embedding(segment)

embeddings = np.nan_to_num(embeddings)

clustering = AgglomerativeClustering(10).fit(embeddings)
labels = clustering.labels_
# print(segments)
for i in range(len(segments)):
  segments[i]["speaker"] = 'SPEAKER ' + str(labels[i] + 1)

def time(secs):
  return datetime.timedelta(seconds=round(secs))

os.remove(path)

output_file = f"{filename_origin}.srt"

srt_entry = ''

subtitle_number = 1

for (i, segment) in enumerate(segments):
    start_time = str(time(segment["start"]))
    end_time = str(time(segment["end"]))
    text = segment["text"][1:] + ' '
    speaker = segment["speaker"]
    srt_entry += f"{subtitle_number}\n{start_time} --> {end_time}\n{speaker}: {text}\n\n"
    subtitle_number += 1

if print_in_console:
    print(srt_entry)
else:
    f = open(f"{filename_origin}.srt", "w", encoding='utf-8')
    f.write(srt_entry)
    f.close()
    print(f"Saved")
