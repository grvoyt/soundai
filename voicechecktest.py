from pyAudioAnalysis import audioSegmentation as aS

# Используем модель VAD для анализа активности голоса
[flags, classes, acc] = aS.mtFileClassification("/Users/grvoyt/webwork/luuv/example1210.wav", "pyAudioAnalysis/data/models/svm_rbf_sm", "svm_rbf")

# flags содержит информацию об активности голоса
print(flags)

# Проверяем, есть ли активность голоса в файле
voice_activity = any(flags)
if voice_activity:
    print("В файле обнаружена активность голоса.")
else:
    print("В файле не обнаружена активность голоса.")