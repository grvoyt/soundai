import os
import warnings
import torch
from pyannote.audio.pipelines.speaker_verification import PretrainedSpeakerEmbedding
from pyannote.audio import Audio
from pyannote.core import Segment
import wave
import contextlib
from sklearn.cluster import AgglomerativeClustering
import numpy as np
import whisper
import datetime
from pathlib import Path

with warnings.catch_warnings():
    warnings.simplefilter("ignore", category=UserWarning)

def make_subtitles(path: str,filename_origin: str):
    print(f"!!!{path} - {filename_origin}")
    srt_path = f"{filename_origin}.srt"
    embedding_model = PretrainedSpeakerEmbedding(
        "speechbrain/spkrec-ecapa-voxceleb",
        device=torch.device("cuda")
    )

    model = whisper.load_model("medium")

    result = model.transcribe(path)
    segments = result["segments"]

    with contextlib.closing(wave.open(path, 'r')) as f:
        frames = f.getnframes()
        rate = f.getframerate()
        duration = frames / float(rate)

    audio = Audio()

    def segment_embedding(segment):
        start = segment["start"]
        # Whisper overshoots the end timestamp in the last segment
        end = min(duration, segment["end"])
        clip = Segment(start, end)
        waveform, sample_rate = audio.crop(path, clip)
        return embedding_model(waveform[None])

    embeddings = np.zeros(shape=(len(segments), 192))
    for i, segment in enumerate(segments):
        embeddings[i] = segment_embedding(segment)

    embeddings = np.nan_to_num(embeddings)

    clustering = AgglomerativeClustering(10).fit(embeddings)
    labels = clustering.labels_
    # print(segments)
    for i in range(len(segments)):
        segments[i]["speaker"] = 'SPEAKER ' + str(labels[i] + 1)

    def time(secs):
        return datetime.timedelta(seconds=round(secs))

    srt_entry = ''

    subtitle_number = 1

    for (i, segment) in enumerate(segments):
        start_time = str(time(segment["start"]))
        end_time = str(time(segment["end"]))
        text = segment["text"][1:] + ' '
        speaker = segment["speaker"]
        srt_entry += f"{subtitle_number}\n{start_time} --> {end_time}\n{speaker}: {text}\n\n"
        subtitle_number += 1


    print(f"Saving {srt_path}")
    f = open(srt_path, "w", encoding='utf-8')
    f.write(srt_entry)
    f.close()

    return Path(srt_path)

